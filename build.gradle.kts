plugins {
	java
	id("org.springframework.boot") version "3.2.0" // Behaviour change was introduced in SB 3.2.0
	id("io.spring.dependency-management") version "1.1.4"
}

group = "com.elderbyte"
version = "0.0.1-SNAPSHOT"

java {
	sourceCompatibility = JavaVersion.VERSION_17
}

repositories {
	mavenCentral()
}

dependencies {

	implementation("org.springframework.boot:spring-boot-starter")
	implementation("org.springframework.boot:spring-boot-starter-data-mongodb")

	// With an older version of the SpringDataMongoDB dependency, the POJO conversion behaves the same as before.
//	implementation("org.springframework.data:spring-data-mongodb:4.1.8")

	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("de.flapdoodle.embed:de.flapdoodle.embed.mongo.spring30x:4.3.2")

}

tasks.withType<Test> {
	useJUnitPlatform()
}
