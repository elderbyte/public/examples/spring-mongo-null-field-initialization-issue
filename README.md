# spring-mongo-null-field-initialization-issue

After SpringBoot 3.1.8 the behaviour of how an objects is loaded and instantiated from the DB seems to have changed.
In previous versions, a field that is initialized within the object had precedence over a 'null' value from the DB.
In SpringBoot 3.2.0, a 'null' value from the DB is set instead of the object's initial value!
