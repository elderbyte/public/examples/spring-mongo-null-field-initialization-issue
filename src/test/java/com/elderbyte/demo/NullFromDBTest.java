package com.elderbyte.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@AutoConfigureDataMongo
class NullFromDBTest {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    @Autowired
    private Repo repo;

    @Autowired
    private MongoTemplate mongoTemplate;

    /***************************************************************************
     *                                                                         *
     * Tests                                                                   *
     *                                                                         *
     **************************************************************************/

    @Test
    public void null_field_in_DB_to_POJO() {

        var id = "id_of_test_object";

        // Save object to DB
        repo.save(new SomeObject(id));

        // Explicitly set field to null
        mongoTemplate.updateFirst(
                new Query(Criteria.where("_id").is(id)),
                new Update().set("someCollection", null),
                SomeObject.class
        );

        // Retrieve object from DB
        var object = repo.findById(id).orElseThrow();

        // Expected behaviour ('null' from DB does not override object's field initialization)
        assertNotNull(object.getSomeCollection());
    }

}
