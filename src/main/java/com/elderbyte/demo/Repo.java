package com.elderbyte.demo;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface Repo extends MongoRepository<SomeObject, String> {

}
